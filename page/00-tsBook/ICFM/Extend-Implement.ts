interface SayName{
    sayName(name:string):void;
}
interface Paobu{
    pao():void;
}
class Nima implements SayName,Paobu{
    sayName(name: string): void {
        alert("我是"+name);
    }

    pao(): void {
        alert("跑了一万公里");
    }
}
//允许多继承
interface Shou extends SayName,Paobu{
    shache():void;
}
//尼玛兽
class NimaShou implements Shou{
    shache(): void {
        alert("尼玛兽刹车了");
    }

    sayName(name: string): void {
        alert("我是尼玛兽");
    }

    pao(): void {
        alert("尼玛兽奔跑了一万公里");
    }
}

// class NimaShenshou extends Nima,NimaShou{}//Classes can only extend a single class

var shou = <Shou>{};//接口对象的形式。
shou.sayName = function (name:string = '兽类') {
    alert(name);
}
shou.sayName("兽类接口");
var go = <Paobu>{ pao: function () {
    alert("跑步");
}}
go.pao();
