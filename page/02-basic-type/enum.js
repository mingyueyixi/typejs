var Student;
(function (Student) {
    Student[Student["xiaoming"] = 0] = "xiaoming";
    Student[Student["xiaohong"] = 1] = "xiaohong";
    Student[Student["xiaolan"] = 2] = "xiaolan";
})(Student || (Student = {}));
;
var xiaomingValue = Student.xiaohong;
//为枚举的元素设置编号
var Color;
(function (Color) {
    Color[Color["Red"] = 1] = "Red";
    Color[Color["Green"] = 4] = "Green";
    Color[Color["Blue"] = 6] = "Blue";
})(Color || (Color = {}));
;
var colorName = Color[4];
function test() {
    document.writeln("xiaoming:" + Student.xiaoming
        + "<p></p>xiaohong:" + Student.xiaohong
        + "<p></p>xiaolan:" + Student.xiaolan);
    document.writeln("<p></p>枚举Color编号为2的元素名称：" + colorName);
}
test();
