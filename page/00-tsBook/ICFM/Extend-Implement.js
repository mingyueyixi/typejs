var Nima = (function () {
    function Nima() {
    }
    Nima.prototype.sayName = function (name) {
        alert("我是" + name);
    };
    Nima.prototype.pao = function () {
        alert("跑了一万公里");
    };
    return Nima;
}());
//尼玛兽
var NimaShou = (function () {
    function NimaShou() {
    }
    NimaShou.prototype.shache = function () {
        alert("尼玛兽刹车了");
    };
    NimaShou.prototype.sayName = function (name) {
        alert("我是尼玛兽");
    };
    NimaShou.prototype.pao = function () {
        alert("尼玛兽奔跑了一万公里");
    };
    return NimaShou;
}());
// class NimaShenshou extends Nima,NimaShou{}//Classes can only extend a single class
var shou = {}; //接口对象的形式。
shou.sayName = function (name) {
    if (name === void 0) { name = '兽类'; }
    alert(name);
};
shou.sayName("兽类接口");
var go = { pao: function () {
        alert("跑步");
    } };
go.pao();
