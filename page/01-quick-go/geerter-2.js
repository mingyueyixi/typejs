//类的使用
var Student = (function () {
    /**
     * 构造函数
     * 其中public的声明是简写的形式，会自动声明具有这个名称的字段。
     */
    function Student(firstName, middleInitial, lastName) {
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
    return Student;
    
}());//调用自身，从而return Student。

function greeter(person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}
var user = new Student("Jane", "M.", "User");
document.body.innerHTML = greeter(user);
