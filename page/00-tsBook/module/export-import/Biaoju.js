"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Email_1 = require("./Email");
/**
 * 镖局
 */
var Biaoju = (function () {
    function Biaoju() {
    }
    /**
     * 押镖:货物
     * 押镖时要发邮件通知委托人
     */
    Biaoju.prototype.yabiao = function (huowu) {
        var email = new Email_1.Email();
        email.sendTo("北京", "我们今天开始押镖。" + Date.now());
        console.log("押镖：" + huowu);
    };
    return Biaoju;
}());
exports.Biaoju = Biaoju;
