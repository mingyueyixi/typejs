"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Email = (function () {
    function Email() {
    }
    Email.prototype.sendTo = function (dress, content) {
        console.log("/n寄往：" + dress + "/n内容：" + content);
        Email.dress = dress;
        Email.content = content;
    };
    Email.prototype.received = function () {
        console.log("/n收到：" + Email.dress + "/n内容：" + Email.content);
        return { d: Email.dress, c: Email.content };
    };
    return Email;
}());
exports.Email = Email;
