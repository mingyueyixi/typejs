//类的使用
class Student {
	fullName: string;//全局变量，类型string
	/**
	 * 构造函数
	 * 其中public的声明是简写的形式，会自动声明具有这个名称的字段。
	 */
	constructor(public firstName, public middleInitial, public lastName) {
		this.fullName = firstName + " " + middleInitial + " " + lastName;
	}
}

interface Person {
	firstName: string;
	lastName: string;
}

function greeter(person: Person) {
	return "Hello, " + person.firstName + " " + person.lastName;
}

var user = new Student("Jane", "M.", "User");

document.body.innerHTML = greeter(user);