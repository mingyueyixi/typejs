/**
 * 类的封装>>>>getter，setter
 */
abstract class Person{
    constructor(public _name:string,public _id:string){}

    //set和get关键字的使用。有些类似于C#
    get name():string{//默认访问权限：public
        return this._name;
    }
    set name(_name:string){
        this._name = _name;
    }
    getName(){
        return this._name;
    }
    setName(_name:string){
        this._name = _name;
}
    abstract doWork();
}
class Student extends Person{
    doWork() {
        console.log("学习");
    }
}
//把类当作接口使用，类的具体实现会被清理成抽象形式。
interface Worker extends  Person{
}




