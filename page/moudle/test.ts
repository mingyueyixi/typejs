import { StringValidator } from "./Validation";//导入字符串验证器
import { ZipCodeValidator } from "./ZipCodeValidator";//导入zipCode验证器
import { LettersOnlyValidator } from "./LettersOnlyValidator";//导入小写验证器

// Some samples to try
let strings = ["Hello", "98052", "101"];

// Validators to use
let validators: { [s: string]: StringValidator; } = {};
validators["ZIP code"] = new ZipCodeValidator();
validators["Letters only"] = new LettersOnlyValidator();

// Show whether each string passed each validator
strings.forEach(s => {
    for (let name in validators) {
        console.log(`"${ s }" - ${ validators[name].isAcceptable(s) ? "matches" : "does not match" } ${ name }`);
    }
});