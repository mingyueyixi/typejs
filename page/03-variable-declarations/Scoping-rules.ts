/**
 * let的作用。
 * 元素的重定义与屏蔽。
 */

class NimaShou{//默认为public修饰
    constructor(public name:string,public id){//public这样的写法会自动生成与赋值给对应的属性（字段）。
    }
    public println(){
        for(let i=0;i<100;i++){
            setTimeout(function () {
                document.write("尼玛兽奔跑了"+i+"公里-------"+"NimaShou:[name:" + this.name + ",id:" + this.id + "]" + "<p>");
            },i*1000);//不会真的给延迟到10秒钟这样的。如果定为10秒，等待10秒后一次性全部输出完毕。
        }
        // console.log("nima");
    }
}
let nima: NimaShou = new NimaShou("尼玛兽",1);
nima.println();