/**
 * Created by Yue on 2017/5/19.
 */
interface LabelledValue {
    id?:string;//可选属性，不一定需要存在
    label: string;

}
function logLabel(labelObj:LabelledValue){
    console.log(labelObj.label);
}
let labelObj_1:LabelledValue = {label:"尼玛兽1",id:'狗头军师'};
let labelObj_2:LabelledValue = {label:"尼玛兽2"};//id是可选的属性，因此即使不存在，也不会报错。
// let labelObj_3:LabelledValue = {id:"狗头军师3"};//label非可选的属性。所以报错。

logLabel(labelObj_1);

