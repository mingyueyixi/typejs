var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 类的封装>>>>getter，setter
 */
var Person = (function () {
    function Person(_name, _id) {
        this._name = _name;
        this._id = _id;
    }
    Object.defineProperty(Person.prototype, "name", {
        //set和get关键字的使用。有些类似于C#
        get: function () {
            return this._name;
        },
        set: function (_name) {
            this._name = _name;
        },
        enumerable: true,
        configurable: true
    });
    Person.prototype.getName = function () {
        return this._name;
    };
    Person.prototype.setName = function (_name) {
        this._name = _name;
    };
    return Person;
}());
var Student = (function (_super) {
    __extends(Student, _super);
    function Student() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Student.prototype.doWork = function () {
        console.log("学习");
    };
    return Student;
}(Person));
