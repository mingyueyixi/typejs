/**
 * 接口、类、函数的定义和一些使用形式
 */

//声音接口
interface Voice {
    getLanguage():string;//默认public
    speak(content:any):void;
}
abstract class Animal implements Voice{
    getLanguage(): string{
        return "动物世界通行语";
    }
    abstract speak(content?: any): void;//可选参数。可选声明的形式为“修饰符 名称?:类型”
}
class Dog extends Animal{//private和protected不能出现在模块或命名空间元素上。此处不允许用来修饰类

//     constructor(public name?:string,public id:string,public age:number,public sex:string){//可选参数不允许在固定参数之前
//         super();
//     }
    //此处使用public，为this.xx = xx的简写。会自动创建并赋值属性
    constructor(public id:string,public age:number,public sex:string,public name?:string){//不允许多个构造函数实现。。。
        super();
    }
    speak(content: any): void {
        alert(content);
    }
    getLanguage(): string{
       return "汪星语";
    }
}
class Cat extends Animal{
    private _name: string;
    speak(content?: any): void {
        alert("喵喵！");
    }
    //getter,setter存取器
    set name(name :string){
        this._name = name;
    }
    get name():string{
        return this._name;
    }
}
class Home{
    // let dogB:Dog = new Dog("001",4,"雌","go");//不能在此使用let，var声明
    private catA:Cat = new Cat();
    private dogA:Dog = new Dog("001",4,"雌","go");
    private readonly tag?:string = "山顶洞人的home";//可选的、只读属性的home。可选声明的形式为“修饰符 名称?:类型”

    protected go(){

    }
    play(monthDay:any){//默认public
        this.catA.name="我是一只喵";//需要使用this.属性来访问，直接访问不到
        this.dogA.name = "我是一只汪";
        console.log(monthDay);
        monthDay = parseInt(monthDay,10);//字符串转成十进制数字
        let select = monthDay % 2;

        switch (select){
            case 0:
                alert("Today,我 play with "+this.dogA.name);
                break;
            case 1:
                alert("Today,我 play with "+this.catA.name);
                break;
            default:
                alert("未知日期,我 play with "+this.dogA.name);
                break;
        }
    }
}
//北京哈巴
function jingbaClick(){
    let jingba:Dog = new Dog("京巴001",4,"雌");
    jingba.speak("汪汪！");
}
function okClick(){
    let home: Home = new Home();
    let day = (<HTMLInputElement> document.getElementById("month-day")).value;//HTMLElement中没有value属性。需要类型断言（强转）
    // let day = (document.getElementById("month-day") as HTMLInputElement).value;//类型断言的另一种方式。
    home.play(day);
}

