"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HomeModule;
(function (HomeModule) {
    var Dog = (function () {
        function Dog() {
        }
        Dog.prototype.say = function () {
            alert("汪星语");
        };
        return Dog;
    }());
    HomeModule.Dog = Dog;
    var Person = (function () {
        function Person() {
        }
        Person.prototype.say = function (content) {
            alert(content);
        };
        return Person;
    }());
    HomeModule.Person = Person;
})(HomeModule || (HomeModule = {}));
var Jiefei_1 = require("./Jiefei");
exports.shanzhai = Jiefei_1.shanzhai;
