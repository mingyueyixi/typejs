
function geerter(person) {
    return "Hello " + person.firstName + "-" + person.lastName;
}
var user = {
    firstName: "Janne",
    lastName: "User"
}; //此对象与Person接口的结构一致。可以无需实现Person就使用其对象。
document.body.innerHTML = geerter(user);
