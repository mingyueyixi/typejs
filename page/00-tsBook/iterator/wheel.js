//迭代器大集合集合
var wheel = function (wheelName, list) {
    var for_of = function () {
        htmlLog("for..of-------");
        for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
            var i = list_1[_i];
            console.log(i); // "4", "5", "6"，值
            htmlLog(i);
        }
    };
    var for_in = function () {
        htmlLog("for..in-------");
        for (var i in list) {
            console.log(i); // "0", "1", "2",索引
            htmlLog(i);
        }
    };
    //普通写法
    var for_ = function () {
        htmlLog("for-------");
        for (var i = 0; i < list.length; i++) {
            var num = list[i];
            console.log(num); // "4", "5", "6"，值
            htmlLog(num);
        }
    };
    var do_while = function () {
        htmlLog("do..while-------");
        var i = 0;
        do {
            console.log(list[i]);
            htmlLog(list[i]);
            i++; //先执行了do块的代码
        } while (i < list.length);
    };
    var while_ = function () {
        htmlLog("while-------");
        var i = 0;
        while (i < list.length) {
            console.log(list[i]);
            htmlLog(list[i]);
            i++;
        }
    };
    //添加文字
    var htmlLog = function (text) {
        var e = document.createElement("p");
        e.textContent = text;
        document.body.appendChild(e);
    };
    switch (wheelName) {
        case "for...of":
            for_of();
            break;
        case "for...in":
            for_in();
            break;
        case "for":
            for_();
            break;
        case "do...while":
            do_while();
            break;
        case "while":
            while_();
            break;
        default:
            break;
    }
};
var list_456 = [4, 5, 6];
var list_12345 = [1, 2, 3, 4, 5];
wheel("for...in", list_456);
wheel("for...of", list_456);
wheel("for", list_456);
wheel("do...while", list_12345);
wheel("while", list_12345);
