/*
 * 使用tsc 命令编译之后，变成普普通通的js文件。没有什么接口的东西。 
 */

//接口的使用
interface Person {
	firstName: string;
	lastName: string;
}

function geerter(person: Person) {
	return "Hello " + person.firstName + "-" + person.lastName;
}
var user = {
	firstName: "Janne",
	lastName: "User"
}; //此对象与Person接口的结构一致。可以无需实现Person就使用其对象。
document.body.innerHTML = geerter(user);