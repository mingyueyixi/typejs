//迭代器大集合集合
let wheel = function (wheelName: string, list: number[]) {

    let for_of = function () {
        htmlLog("for..of-------")
        for (let i of list) {
            console.log(i); // "4", "5", "6"，值
            htmlLog(i);
        }
    }
    let for_in = function () {
        htmlLog("for..in-------")
        for (let i in list) {
            console.log(i); // "0", "1", "2",索引
            htmlLog(i);
        }
    }

//普通写法
    let for_ = function () {
        htmlLog("for-------")

        for (let i = 0; i < list.length; i++) {
            let num = list[i];
            console.log(num);// "4", "5", "6"，值
            htmlLog(num);
        }
    }

    let do_while = () => {
        htmlLog("do..while-------")
        let i = 0;
        do {
            console.log(list[i]);
            htmlLog(list[i]);
            i++;//先执行了do块的代码
        } while (i < list.length);
    };
    let while_ = () => { //简单来说就是解决this指向不明的问题。
        htmlLog("while-------")
        let i = 0;
        while (i < list.length) {
            console.log(list[i]);
            htmlLog(list[i]);
            i++;
        }
    }
    //添加文字
    let htmlLog = function (text: any) {
        let e = document.createElement("p");
        e.textContent = text;
        document.body.appendChild(e);
    }
    switch (wheelName) {

        case "for...of":
            for_of();
            break;
        case "for...in":
            for_in();
            break;
        case "for":
            for_();
            break;
        case "do...while":
            do_while();
            break;
        case "while":
            while_();
            break;
        default:
            break;
    }
}
let list_456 = [4, 5, 6];
let list_12345 = [1, 2, 3, 4, 5];
wheel("for...in", list_456);
wheel("for...of", list_456);
wheel("for", list_456);
wheel("do...while", list_12345);
wheel("while", list_12345);

