/*
 * 数组array类型
 */
let list_1: number[] = [0, 1, 2, 3];
let list_2: number[] = [4, 5, 6, 7, 8, 9];

//此处有怪异情况
function test() {
    // for (let item in list_1) {
    //     document.write("<p>" + item);
    // }

    // for (let it in list_2) {
    //     document.write("<P>" + it);
    // }
    let print_1 = function (){
        for (let item in list_1) {
            document.write("<p>" + item);
        }
    };
    let print_2 = function () {
        for (let it in list_2) {
            document.write("<P>" + it);
        }
    };
    print_1();
    print_2();
}


test();