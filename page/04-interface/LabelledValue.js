function logLabel(labelObj) {
    console.log(labelObj.label);
}
var labelObj_1 = { label: "尼玛兽1", id: '狗头军师' };
var labelObj_2 = { label: "尼玛兽2" }; //id是可选的属性，因此即使不存在，也不会报错。
// let labelObj_3:LabelledValue = {id:"狗头军师3"};//label非可选的属性。所以报错。
logLabel(labelObj_1);
