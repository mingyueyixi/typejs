/**
 * 接口、类、函数的定义和一些使用形式
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Animal = (function () {
    function Animal() {
    }
    Animal.prototype.getLanguage = function () {
        return "动物世界通行语";
    };
    return Animal;
}());
var Dog = (function (_super) {
    __extends(Dog, _super);
    //     constructor(public name?:string,public id:string,public age:number,public sex:string){//可选参数不允许在固定参数之前
    //         super();
    //     }
    //此处使用public，为this.xx = xx的简写。会自动创建并赋值属性
    function Dog(id, age, sex, name) {
        var _this = _super.call(this) || this;
        _this.id = id;
        _this.age = age;
        _this.sex = sex;
        _this.name = name;
        return _this;
    }
    Dog.prototype.speak = function (content) {
        alert(content);
    };
    Dog.prototype.getLanguage = function () {
        return "汪星语";
    };
    return Dog;
}(Animal));
var Cat = (function (_super) {
    __extends(Cat, _super);
    function Cat() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Cat.prototype.speak = function (content) {
        alert("喵喵！");
    };
    Object.defineProperty(Cat.prototype, "name", {
        get: function () {
            return this._name;
        },
        //getter,setter存取器
        set: function (name) {
            this._name = name;
        },
        enumerable: true,
        configurable: true
    });
    return Cat;
}(Animal));
var Home = (function () {
    function Home() {
        // let dogB:Dog = new Dog("001",4,"雌","go");//不能在此使用let，var声明
        this.catA = new Cat();
        this.dogA = new Dog("001", 4, "雌", "go");
        this.tag = "山顶洞人的home"; //可选的、只读属性的home。可选声明的形式为“修饰符 名称?:类型”
    }
    Home.prototype.go = function () {
    };
    Home.prototype.play = function (monthDay) {
        this.catA.name = "我是一只喵"; //需要使用this.属性来访问，直接访问不到
        this.dogA.name = "我是一只汪";
        console.log(monthDay);
        monthDay = parseInt(monthDay, 10); //字符串转成十进制数字
        var select = monthDay % 2;
        switch (select) {
            case 0:
                alert("Today,我 play with " + this.dogA.name);
                break;
            case 1:
                alert("Today,我 play with " + this.catA.name);
                break;
            default:
                alert("未知日期,我 play with " + this.dogA.name);
                break;
        }
    };
    return Home;
}());
//北京哈巴
function jingbaClick() {
    var jingba = new Dog("京巴001", 4, "雌");
    jingba.speak("汪汪！");
}
function okClick() {
    var home = new Home();
    var day = document.getElementById("month-day").value; //HTMLElement中没有value属性。需要类型断言（强转）
    // let day = (document.getElementById("month-day") as HTMLInputElement).value;//类型断言的另一种方式。
    home.play(day);
}
