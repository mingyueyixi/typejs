import { StringValidator } from "./Validation";

const lettersRegexp = /^[A-Za-z]+$/;//正则表达式

export class LettersOnlyValidator implements StringValidator {
    isAcceptable(s: string) {
        return lettersRegexp.test(s);
    }
}