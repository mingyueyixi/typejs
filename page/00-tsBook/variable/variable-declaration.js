var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
gog = "迷你";
var gog;
if (true) {
    alert(gog);
}
function forStrLet() {
    var str = ["草原", "一万只尼玛兽呼啸而过"];
    for (var i = 0; i < str[0].length; i++) {
        setElement("p", "------" + str[0].charAt(i) + "------");
        for (var i_1 = 0; i_1 < str[1].length; i_1++) {
            setElement("p", str[1].charAt(i_1)); //这里的i是里层for定义的i，假如都使用var声明，则会里外for互相覆盖，导致混乱。
        }
    }
}
function forStrVar() {
    var str = ["草原", "一万只尼玛兽呼啸而过"];
    for (var i = 0; i < str[0].length; i++) {
        setElement("p", "------" + str[0].charAt(i) + "------");
        //里层第一次循环完毕后，i==str[1]的长度-1，覆盖外层循环，导致外层循环条件为false，于是外层循环只执行一次
        for (var i = 0; i < str[1].length; i++) {
            setElement("p", str[1].charAt(i)); //contents += str.charAt(i);//var声明的i互相干架。
        }
    }
}
//此方法的作用：在body标签中追加指定元素和内容
function setElement(elementName, contentText) {
    var e = document.createElement(elementName);
    e.textContent = contentText;
    document.body.appendChild(e);
}
setElement("h1", "let声明：");
forStrLet();
setElement("h1", "var声明：");
forStrVar();
//数组的解构
var input = [1, 2];
var first = input[0], second = input[1]; //相当于使用索引声明两个变量first = input[0];second = input[1];
setElement("h1", "数组的解构");
setElement("p", first + "----" + second);
//交换值
_a = [first, second], second = _a[0], first = _a[1];
setElement("p", first + "----" + second);
//...的形式声明剩余变量。
var _b = [1, 2, 3, 4, 5], o = _b[0], u = _b[2], t = _b.slice(3); //一些元素可以不必命名
setElement("p", o + "----" + u + "----" + t);
//对象的解构
var abcd = { a: "foo", b: 12, c: "bar", d: false }; //符号:在此处的作用不是声明类型，而是声明属性的值。
var a = abcd.a, b = abcd.b; //虽然abc的属性更多。匿名的声明
// let { b, ...v} = abcd;//起始元素必须是abcd的一个属性。
var ag = abcd;
setElement("h1", "对象的解构");
setElement("p", ag.a);
//属性的重命名
var newA = abcd.a, newB = abcd.b;
setElement("p", newA); //使用新的名称
function f(_a) {
    var a = _a.a, b = _a.b;
    // ...
}
//数组的展开
var num_a = [1, 2, 3];
var num_b = num_a.concat([4, 5, "hao"]); //...展开为元组对象
var num_c = [num_a, 4, 5, "hao"]; //展开
setElement("h1", "展开数组");
setElement("p", num_b);
//对象的展开
var dog = { id: 1, name: "dog" };
var home = __assign({ father: "da", monther: "mo", me: "hao" }, dog, { name: "狗狗阿里" }); //使用"...对象"写法展开。后声明的属性允许覆盖
setElement("h1", "展开对象");
setElement("p", home.name);
var _a;
