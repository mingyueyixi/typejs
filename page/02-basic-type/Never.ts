/*
 * Never类型表示永远不存在值。可用于总是抛出异常等导致的无返回值的方法。
 * Never类型是所有类型的子类型，但它自己没有子类型，因此，它除了自己，不能被任何类型赋值，即使是any型,undefied型：
 * let ne:never = undefied；//error
 */
// 返回never的函数必须存在无法达到的终点
function error(message: string): never {
    document.write(message+":error");
    throw new Error(message);
}

// 推断的返回值类型为never
function fail() {
    return error("Never：Something failed | ");
}

// 返回never的函数必须存在无法达到的终点
function infiniteLoop(): never {
    let count:number = 0;
    while (true) {
    document.writeln("Never over");
    }
}
function neverDo(){
    let ni:never;
    let ne:never = ni ;//除了never，不支持其它任何类型的赋值行为，包括undefied;
    document.write("never类型默认值："+ni);//但是输出时默认的值是undefied

}
function test(){
    // error("Never");
    // fail();
    // infiniteLoop();
    neverDo();
}

test();

