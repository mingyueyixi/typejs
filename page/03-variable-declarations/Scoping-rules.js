/**
 * let的作用。
 * 元素的重定义与屏蔽。
 */
var NimaShou = (function () {
    function NimaShou(name, id) {
        this.name = name;
        this.id = id;
    }
    NimaShou.prototype.println = function () {
        var _loop_1 = function (i) {
            setTimeout(function () {
                document.write("尼玛兽奔跑了" + i + "公里-------" + "NimaShou:[name:" + this.name + ",id:" + this.id + "]" + "<p>");
            }, i * 1000); //不会真的给延迟到10秒钟这样的。如果定为10秒，等待10秒后一次性全部输出完毕。
        };
        for (var i = 0; i < 100; i++) {
            _loop_1(i);
        }
        // console.log("nima");
    };
    return NimaShou;
}());
var nima = new NimaShou("尼玛兽", 1);
nima.println();
