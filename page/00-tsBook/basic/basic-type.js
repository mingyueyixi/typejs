//作用：添加按钮到body标签中
var putButton = function (text, info) {
    var button = document.createElement("button");
    var p = document.createElement("p");
    button.textContent = text;
    document.body.appendChild(button);
    document.body.appendChild(p);
    button.onclick = function () {
        alert(info);
    };
};
//boolean类型
var flag;
var flag_1 = false;
putButton("let flag:boolean;", flag);
putButton("let flag_1:boolean = false;", flag_1);
//number
var PI = 3.1415926535897932384626433; //圆周率
putButton("number>>>圆周率PI", PI);
putButton("number>>>圆直径5，周长", 5 * PI);
//字符串
var str_double_quotes = "双引号声明string";
var str_single_quote = '单引号声明string';
var str_accent = "\u4E0A\u70B9\u53F7\u58F0\u660Estring"; //可内嵌${表达式}
putButton("string>>>" + str_accent + "${5*8}", "5*8\u7684\u503C\uFF1A" + 5 * 8);
//数组,两种声明方式
var nums = [1, 2, 3, 4];
var numList = nums;
putButton("数组[1,2,3,4]", numList.toString());
//元组Tuple
var dog = ["蝴蝶犬", 5]; //元组是一个数组，允许元素类型不同
dog[3] = "岁了"; //数组越界不报错，因为"dog"是string|number联合类型
// dog[4] = false;//错误
putButton("元组Tuple", dog[0] + dog[1] + dog[3]);
//enum 枚举
var Xiyouji;
(function (Xiyouji) {
    Xiyouji[Xiyouji["\u5510\u50E7"] = 0] = "\u5510\u50E7";
    Xiyouji[Xiyouji["\u609F\u7A7A"] = 1] = "\u609F\u7A7A";
    Xiyouji[Xiyouji["\u516B\u6212"] = 3] = "\u516B\u6212";
    Xiyouji[Xiyouji["\u6C99\u50E7"] = 4] = "\u6C99\u50E7";
    Xiyouji[Xiyouji["\u767D\u9F99\u9A6C"] = 5] = "\u767D\u9F99\u9A6C";
})(Xiyouji || (Xiyouji = {})); //手动指定索引下标
var tangseng = Xiyouji.唐僧;
var wukong = Xiyouji[1]; //手动声明索引，左边索引从零开始
var shaseng = Xiyouji[4]; //手动声明索引，右边索引依次增加
putButton("enum>>>西游记", wukong);
//any 任意值
var 水牛 = "shuiniu";
var shuiniu = 100;
水牛 = shuiniu;
putButton("any>>>水牛", 水牛);
//void 空值，只能为null或undefined
var v = null;
v = undefined;
function f() { return v; }
; //空返回值，有点奇怪
putButton("void>>>", f());
//null和undefined，默认情况下null和undefined是所有类型的子类型。其他类型的值一般都可以赋给它们。
var undef = null;
var nul = undefined;
putButton("null和undefined", "null和undefined是所有类型的子类型");
//Never类型表示的是那些永不存在的值的类型
//never类型是任何类型的子类型，但没有类型是never的子类型
// 这意味着never可以赋值给任何类型，但没有任何类型可以赋值给never类型（除了never本身之外）
var neve; //初值undefined
var go = neve;
//never表示永远不存在值的类型
function trueWhile() {
    while (true) { }
}
putButton("never>>>", go);
