/*
 * 数组array类型
 */
var list_1 = [0, 1, 2, 3];
var list_2 = [4, 5, 6, 7, 8, 9];
//此处有怪异情况
function test() {
    // for (let item in list_1) {
    //     document.write("<p>" + item);
    // }
    // for (let it in list_2) {
    //     document.write("<P>" + it);
    // }
    var print_1 = function () {
        for (var item in list_1) {
            document.write("<p>" + item);
        }
    };
    var print_2 = function () {
        for (var it in list_2) {
            document.write("<P>" + it);
        }
    };
    print_1();
    print_2();
}
test();
